\documentclass{beamer}
%\usetheme{Warsaw}
\usepackage[utf8]{inputenc}

\usepackage{fancyvrb}
\input{pygmentize.py_tex}
\usepackage{xspace}
\newcommand{\Cpp}{C\protect\raisebox{.18ex}{++}\xspace}

\title[mpi4py]{MPI for Python}
\subtitle{\url{http://mpi4py.scipy.org}}
\author[L.~Dalcin]{Lisandro~Dalcin\\\url{dalcinl@gmail.com}}
\institute[]
{
  Centro Internacional de Métodos Computacionales en Ingeniería\\
  Consejo Nacional de Investigaciones Científicas y Técnicas\\
  Santa Fe, Argentina
}
\date [PASI '11]
{
  January, 2011\\
  Python for parallel scientific computing\\
  PASI, Valparaíso, Chile
}

\AtBeginSection[]
{
  \begin{frame}
    \tableofcontents[currentsection]
  \end{frame}
}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section*{Outline}
\begin{frame}
  \frametitle{Outline}
  \tableofcontents
\end{frame}

% --- Overview ---

\section{Overview}

\begin{frame}
  \frametitle{What is \textbf{mpi4py}?}
  \begin{itemize}
  \item Full-featured Python bindings for
    \href{http://www.mpi-forum.org}{\textbf{MPI}}.
  \item API based on the standard MPI-2 \Cpp bindings.
  \item Almost all MPI calls are supported.
    \begin{itemize}
    \item targeted to MPI-2 implementations.
    \item also works with MPI-1 implementations.
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Implementation}
  Implemented with \href{http://www.cython.org}{\textbf{Cython}}
  \begin{itemize}
  \item Code base far easier to write, maintain, and extend.
  \item Faster than other solutions (mixed Python and C codes).
  \item A \textsl{pythonic} API that runs at C speed !
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Features -- MPI-1}
  \begin{itemize}
  \item Process groups and communication domains.
    \begin{itemize}
    \item intracommunicators
    \item intercommunicators
    \end{itemize}
  \item Point to point communication.
    \begin{itemize}
    \item blocking (send/recv)
    \item nonblocking (isend/irecv + test/wait)
    \end{itemize}
  \item Collective operations.
    \begin{itemize}
    \item Synchronization (barrier)
    \item Communication (broadcast, scatter/gather)
    \item Global reductions (reduce, scan)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Features -- MPI-2}
  \begin{itemize}
  \item Dynamic process management (spawn, connect/accept).
  \item Parallel I/O (read/write).
  \item One sided operations, a.k.a. RMA (put/get/accumulate).
  \item Extended collective operations.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Features -- Python}
  \begin{itemize}
  \item Communication of Python objects.
    \begin{itemize}
    \item high level and very convenient, based in \texttt{pickle}
      serialization
    \item can be slow for large data (CPU and memory consuming)
    \end{itemize}
    \fbox{\texttt{<object>} $\longrightarrow{}$ \texttt{pickle.dump()}
      $\longrightarrow{}$ \texttt{send()}}
    \\\hspace{39ex}$\downarrow{}$\\
    \fbox{\texttt{<object>} $\longleftarrow{}$ \texttt{pickle.load()}
      $\longleftarrow{}$ \texttt{recv()}}
  \item Communication of array data
    (e.g. \href{http://numpy.scipy.org}{\textbf{NumPy}} arrays).
    \begin{itemize}
    \item lower level, slightly more verbose
    \item very fast, almost C speed (for messages above 5-10 KB)
    \end{itemize}
    \fbox{message = \texttt{[<object>, (count, displ), datatype]}}
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    Point to Point Throughput -- Gigabit Ethernet
  \end{center}
  \includegraphics[scale=0.5]{mpi4py_ge_PingPong_BW.pdf}
\end{frame}
\begin{frame}
  \begin{center}
    Point to Point Throughput -- Shared Memory
  \end{center}
  \includegraphics[scale=0.5]{mpi4py_sm_PingPong_BW.pdf}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Features -- IPython}
  Integration with \href{http://ipython.scipy.org}{\textbf{IPython}}
  enables MPI to be used \emph{interactively}.
  \begin{itemize}
  \item Start engines with MPI enabled
\begin{verbatim}
   $ ipcluster mpiexec -n 16 --mpi=mpi4py
\end{verbatim}
  \item Connect to the engines
\begin{verbatim}
   $ ipython
   In [1]: from IPython.kernel import client
   In [2]: mec = client.MultiEngineClient()
   In [3]: mec.activate()
\end{verbatim}
  \item Execute commands using \texttt{\%px}
\begin{verbatim}
   In [4]: %px from mpi4py import MPI
   In [5]: %px print(MPI.Get_processor_name())
\end{verbatim}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Features -- Interoperability}
  Good support for wrapping other MPI-based codes.
  \begin{itemize}
  \item You can use \href{http://cython.org}{\textbf{Cython}}
    (\texttt{cimport} statement).
  \item You can use \href{http://swig.org}{\textbf{SWIG}}
    (\textsl{typemaps} provided).
  \item You can use \href{http://f2py.com}{\textbf{F2Py}}
    (\texttt{py2f()}/\texttt{f2py()} methods).
  \item You can use
    \href{http://www.boost.org}{\textbf{Boost::Python}} or
    \textbf{hand-written C} extensions.
  \end{itemize}
  \textbf{mpi4py} will allow you to use virtually any MPI based
  C/\Cpp/Fortran code from Python.
\end{frame}

\begin{frame}
  \frametitle{Hello World!}
  \input{helloworld.py_tex}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Hello World! -- Wrapping with \textbf{SWIG}}
  \begin{columns}[t]
    \column{.45\textwidth}
    \begin{block}{C source}
      {\footnotesize \input{helloworld_swig.c_tex}}
    \end{block}
    \column{.45\textwidth}
    \begin{block}{SWIG interface file}
      {\footnotesize \input{helloworld_swig.i_tex}}
    \end{block}
  \end{columns}
  \begin{block}{At the Python prompt \ldots}
    \footnotesize
\begin{verbatim}
>>> from mpi4py import MPI
>>> import helloworld
>>> helloworld.sayhello(MPI.COMM_WORLD)
Hello, World! I am process 0 of 1.
\end{verbatim}
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Hello World! -- Wrapping with \textbf{Boost.Python}}
  {\footnotesize \input{helloworld_boost.cxx_tex}}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Hello World! -- Wrapping with \textbf{F2Py}}
  \begin{block}{Fortran 90 source}
    {\footnotesize \input{helloworld_f2py.f90_tex}}
  \end{block}
  \begin{block}{At the Python prompt \ldots}
    \footnotesize
\begin{verbatim}
>>> from mpi4py import MPI
>>> import helloworld
>>> fcomm = MPI.COMM_WORLD.py2f()
>>> helloworld.sayhello(fcomm)
Hello, World! I am process 0 of 1.
\end{verbatim}
  \end{block}
\end{frame}


% --- Communicators ---


\section{Communicators}

\begin{frame}
  \frametitle{Communicators}
  \fbox{communicator = process group + communication context}
  \linebreak
  \begin{itemize}
  \item Predefined instances
    \begin{itemize}
    \item \texttt{COMM\_WORLD}
    \item \texttt{COMM\_SELF}
    \item \texttt{COMM\_NULL}
    \end{itemize}
  \item Accessors
    \begin{itemize}
    \item \texttt{rank = comm.Get\_rank() \# or comm.rank}
    \item \texttt{size = comm.Get\_size() \# or comm.size}
    \item \texttt{group = comm.Get\_group()}
    \end{itemize}
  \item Constructors\\
    \begin{itemize}
    \item \texttt{newcomm = comm.Dup()}
    \item \texttt{newcomm = comm.Create(group)} 
    \item \texttt{newcomm = comm.Split(color, key)}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[t]
  \frametitle{Communicators -- \texttt{Create()}}
  \input{comm_create.py_tex}
\end{frame}

\begin{frame}[t]
  \frametitle{Communicators -- \texttt{Split()}}
  \input{comm_split.py_tex}
\end{frame}

\begin{frame}
  \frametitle{Exercise \#1}
  \begin{enumerate}[a)]
  \item Create a new process group containing the processes in the
    group of \texttt{COMM\_WORLD} with \textbf{even} rank. Use the new
    group to create a new communicator.\\
    \textbf{Tip}: use \texttt{Group.Incl()} or \texttt{Group.Range\_incl()}
  \item Use \texttt{Comm.Split()} to split \texttt{COMM\_WORLD} in two halves.
    \begin{itemize}
    \item The first half contains the processes with \textbf{even} rank
      in \texttt{COMM\_WORLD}. The process rank ordering in the new
      communication is \textbf{ascending}.
    \item The second half contains the processes with \textbf{odd} rank
      in \texttt{COMM\_WORLD}. The process rank ordering in the new
      communication is \textbf{descending}.
  \end{itemize}
  \end{enumerate}
\end{frame}

% --- Point to Point ---

\section{Point to Point Communication}

\begin{frame}[fragile]
  \begin{itemize}
  \item Blocking communication
    \begin{itemize}
    \item Python objects
      {\footnotesize
\begin{verbatim}
comm.send(obj, dest=0, tag=0)
obj = comm.recv(None, src=0, tag=0)
\end{verbatim}
      }
    \item Array data
      {\footnotesize
\begin{verbatim}
comm.Send([array, count, datatype], dest=0, tag=0)
comm.Recv([array, count, datatype], src=0, tag=0)
\end{verbatim}
      }
    \end{itemize}
  \item Nonblocking communication
    \begin{itemize}
    \item Python objects
      {\footnotesize
\begin{verbatim}
request = comm.isend(object, dest=0, tag=0)}
request.Wait()
\end{verbatim}
      }
    \item Array data
      {\footnotesize
\begin{verbatim}
req1 = comm.Isend([array, count, datatype], dest=0, tag=0)
req2 = comm.Irecv([array, count, datatype], src=0, tag=0)
MPI.Request.Waitall([req1, req2])}
\end{verbatim}
      }
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{PingPong}
  \input{p2p_pingpong.py_tex}
\end{frame}

\begin{frame}
  \frametitle{PingPing}
  \input{p2p_pingping.py_tex}
\end{frame}

\begin{frame}
  \frametitle{Exchange}
  \input{p2p_exchange.py_tex}
\end{frame}

\begin{frame}
  \frametitle{PingPing with NumPy arrays}
  \input{p2p_pingping-numpy.py_tex}
\end{frame}

\begin{frame}
  \frametitle{Exercise \#2}
  \begin{enumerate}[a)]
  \item Modify \emph{PingPong} example to communicate NumPy arrays.\\
    \textbf{Tip}: use \texttt{Comm.Send()} and \texttt{Comm.Recv()}
  \item Modify \emph{Exchange} example to communicate NumPy arrays.\\
    Use nonblocking communication for both sending and receiving.\\
    \textbf{Tip}: use \texttt{Comm.Isend()} and \texttt{Comm.Irecv()}
  \end{enumerate}
\end{frame}

% --- Collectives ---

\section{Collective Operations}

\begin{frame}
  \includegraphics[scale=0.75]{collectives1.pdf}
\end{frame}

\begin{frame}
  \includegraphics[scale=0.75]{collectives2.pdf}
\end{frame}

\begin{frame}
  \frametitle{Broadcast}
  \input{coll_bcast.py_tex}
\end{frame}

\begin{frame}
  \frametitle{Scatter}
  \input{coll_scatter.py_tex}
\end{frame}

\begin{frame}
  \frametitle{Gather \& Gather to All}
  \input{coll_gather.py_tex}
\end{frame}

\begin{frame}
  \frametitle{Reduce \& Reduce to All}
  \input{coll_reduce.py_tex}
\end{frame}

\begin{frame}
  \frametitle{Exercise \#3}
  \begin{enumerate}[a)]
  \item Modify \emph{Broadcast}, \emph{Scatter}, and \emph{Gather}
    example to communicate NumPy arrays.
  \item Write a routine implementing parallel
    \emph{matrix}--\emph{vector} product
    \texttt{y~=~matvec(comm,A,x)}. 
    \begin{itemize}
    \item the global matrix is dense and square.
    \item matrix rows and vector entries have matching block
      distribution.
    \item all processes own the same number of matrix rows.
    \end{itemize}
    \textbf{Tip}: use \texttt{Comm.Allgather()} and \texttt{numpy.dot()}
  \end{enumerate}
\end{frame}

% --- Compute Pi ---

\section{Compute Pi}

\begin{frame}
  \frametitle{Compute Pi}
  \Large
  \begin{equation*}
    \pi =
    \int_0^1 \frac{4}{1+x^2} dx \approx
    \frac{1}{n}\sum_{i=0}^{n-1}\frac{4}{1+(\frac{i+0.5}{n})^2}
  \end{equation*}
\end{frame}

\begin{frame}[t]
  \frametitle{Compute Pi -- sequential}
  \input{compute_pi-seq.py_tex}
\end{frame}

\begin{frame}[t]
  \frametitle{Compute Pi -- parallel [1]}
  \input{compute_pi-mpi-1.py_tex}
\end{frame}

\begin{frame}[t]
  \frametitle{Compute Pi -- parallel [2]}
  \input{compute_pi-mpi-2.py_tex}
\end{frame}

\begin{frame}
  \frametitle{Exercise \#4}
  Modify \emph{Compute Pi} example to employ NumPy.\\ 
  \smallskip\smallskip
  \textbf{Tip}: you can convert a Python \texttt{int}/\texttt{float}
  object to a NumPy \emph{scalar} with \texttt{x~=~numpy.array(x)}.
\end{frame}

% --- Mandelbrot ---

\section{Mandelbrot Set}

\begin{frame}
  \frametitle{Mandelbrot Set}
  \includegraphics[scale=0.5]{mandelbrot.pdf}
\end{frame}

\begin{frame}[t]
  \frametitle{Mandelbrot Set -- sequential [1]}
  \input{mandelbrot-seq-1.py_tex}
\end{frame}

\begin{frame}[t]
  \frametitle{Mandelbrot Set -- sequential [2]}
  \input{mandelbrot-seq-2.py_tex}
\end{frame}

\begin{frame}
  \frametitle{Mandelbrot Set -- partitioning}
  \begin{minipage}{\textwidth}
    \centering
    \includegraphics[scale=0.25]{mandelbrot.pdf}
  \end{minipage}
  \begin{columns}[t]
    \begin{column}{0.45\textwidth}
      \centering
      \includegraphics[scale=0.5]{block.pdf}\\
      Block distribution
    \end{column}
    \begin{column}{0.45\textwidth}
      \centering
      \includegraphics[scale=0.5]{cyclic.pdf}\\
      Cyclic distribution
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[t]
  \frametitle{Mandelbrot Set -- parallel, block [1]}
  \input{mandelbrot-mpi-block-1.py_tex}
\end{frame}
\begin{frame}[t]
  \frametitle{Mandelbrot Set -- parallel, block [2]}
  \input{mandelbrot-mpi-block-2.py_tex}
\end{frame}
\begin{frame}[t]
  \frametitle{Mandelbrot Set -- parallel, block [3]}
  \input{mandelbrot-mpi-block-3.py_tex}
\end{frame}
\begin{frame}[t]
  \frametitle{Mandelbrot Set -- parallel, block [4]}
  \input{mandelbrot-mpi-block-4.py_tex}
\end{frame}
\begin{frame}[t]
  \frametitle{Mandelbrot Set -- parallel, block [5]}
  \input{mandelbrot-mpi-block-5.py_tex}
\end{frame}

\begin{frame}
  \frametitle{Exercise \#5}%
  Measure the wall clock time $T_i$ of local computations at each
  process for the \emph{Mandelbrot Set} example with \textbf{block} and
  \textbf{cyclic} row distributions.\\
  What row distribution is better regarding load balancing?\\
  \smallskip\smallskip
  \textbf{Tip}: use \texttt{Wtime()} to measure wall time, compute
  the ratio $T_\text{max}/T_\text{min}$ to compare load balancing.
\end{frame}


% --- DynProc ---

\section{Dynamic Process Management}

\begin{frame}
  \frametitle{Dynamic Process Management}
  \begin{itemize}
  \item Useful in assembling complex distributed applications. Can
    couple \textbf{independent parallel codes} written in
    \textbf{different languages}.
  \item Create new processes from a running program.\\
    -- \texttt{Comm.Spawn()} and \texttt{Comm.Get\_parent()}
  \item Connect two running applications together.\\
    -- \texttt{Comm.Connect()} and \texttt{Comm.Accept()}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Dynamic Process Management -- Spawning} Spawning new
  processes is a \emph{collective operation} that creates an
  \textbf{intercommunicator}.
  \begin{itemize}
  \item Local group is group of spawning processes (parent).
  \item Remote group is group of new processes (child).
  \item \texttt{Comm.Spawn()} lets parent processes spawn the child
    processes. It returns a new intercommunicator. 
  \item \texttt{Comm.Get\_parent()} lets child processes find
    intercommunicator to the parent group. Child processes have own
    \texttt{COMM\_WORLD}.
  \item \texttt{Comm.Disconnect()} ends the parent--child
    connection. After that, both groups can continue running.
  \end{itemize}
\end{frame}

\begin{frame}[t]
  \frametitle{Dynamic Process Management -- Compute Pi (parent)}
  \small\input{compute_pi-parent.py_tex}
\end{frame}

\begin{frame}[t]
  \frametitle{Dynamic Process Management -- Compute Pi (child)}
  \small\input{compute_pi-child.py_tex}
\end{frame}

\begin{frame}
  \frametitle{Exercise \#5}
  \begin{enumerate}[a)]
  \item Implement the \emph{Compute Pi} \textbf{child} code in
    \textbf{C} or \textbf{\Cpp}. Adjust the parent code in Python to
    spawn the new implementation.
  \item Compute and plot the \emph{Mandelbrot Set} using spawning with
    parent/child codes implemented in Python.\\
    \textbf{Tip}: Reuse the provided parent code in Python and
    translate the child code in Fortran 90 to Python.
  \end{enumerate}
\end{frame}


% --- Closing ---

\section*{Closing}

\begin{frame}
  \Large
  Do not hesitate to ask for help \ldots
  \begin{itemize}
  \item Mailing List: \url{mpi4py@googlegroups.com}
  \item Mail\&Chat:   \url{dalcinl@gmail.com}
  \end{itemize}
  \bigskip
  \begin{centering}
   \Huge Thanks!\par
  \end{centering}
\end{frame}

\end{document}


% Local Variables:
% mode: latex
% TeX-PDF-mode: t
% End:
